<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Game;
use AppBundle\Game\GameContext;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameContextTest extends TestCase
{
    public function testLoadGameWhenNoSession()
    {
        $session = $this->prophesize(SessionInterface::class);
        $session->get('hangman')->willReturn(false);

        $gameContext = new GameContext(
            $session->reveal()
        );
        $result = $gameContext->loadGame();

        $this->assertFalse($result);
    }

    public function testLoadGameWhenAlreadyStarted()
    {
        $session = $this->prophesize(SessionInterface::class);
        $session->get('hangman')->willReturn([
            'word' => 'test',
            'attempts' => 4,
            'tried_letters' => ['a'],
            'found_letters' => ['t'],
        ]);

        $gameContext = new GameContext(
            $session->reveal()
        );
        $result = $gameContext->loadGame();

        $this->assertInstanceOf(Game::class, $result);
    }
}
