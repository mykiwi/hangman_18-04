<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Game;
use AppBundle\Game\GameContextInterface;
use AppBundle\Game\GameRunner;
use AppBundle\Game\WordListInterface;
use PHPUnit\Framework\TestCase;

class GameRunnerTest extends TestCase
{
    public function testPlayLetter()
    {
        $context = $this->prophesize(GameContextInterface::class);
        $wordList = $this->prophesize(WordListInterface::class);

        $gameRunner = new GameRunner(
            $context->reveal(),
            $wordList->reveal()
        );

        $game = $this->prophesize(Game::class);
        $game->tryLetter('L');

        $context->loadGame()->willReturn($game->reveal());
        $context->save($game->reveal())->shouldBeCalled();

        $this->assertInstanceOf(
            Game::class,
            $gameRunner->playLetter('L')
        );
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage No game context set.
     */
    public function testPlayLetterWhenNoGame()
    {
        $context = $this->prophesize(GameContextInterface::class);
        $wordList = $this->prophesize(WordListInterface::class);

        $gameRunner = new GameRunner(
            $context->reveal(),
            $wordList->reveal()
        );

        $context->loadGame()->willReturn(false);
        $gameRunner->playLetter('L');
    }
}
