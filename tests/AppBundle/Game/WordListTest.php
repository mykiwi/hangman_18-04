<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Loader\LoaderInterface;
use AppBundle\Game\WordList;
use PHPUnit\Framework\TestCase;

class WordListTest extends TestCase
{
    public function testGetRandom()
    {
        $mockLoader = $this->prophesize(LoaderInterface::class);
        $mockLoader->load('example.txt')->willReturn(['test']);

        $wordList = new WordList();
        $wordList->addLoader('txt', $mockLoader->reveal());
        $wordList->loadDictionaries(['example.txt']);

        $this->assertSame('test', $wordList->getRandomWord(4));
    }
}
