<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Game;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    public function testTryWordWithGoodWord()
    {
        $game = new Game('test');
        $this->assertTrue($game->tryWord('test'));
    }

    public function testTryWordWithWrongWord()
    {
        $game = new Game('test');
        $this->assertFalse($game->tryWord('coucou'));
    }

    public function testGetContext()
    {
        $game = new Game('coucou', 2, ['a'], ['o']);
        $this->assertAttributeSame('coucou', 'word', $game);
        $this->assertAttributeSame(2, 'attempts', $game);
        $this->assertAttributeSame(['a'], 'triedLetters', $game);
        $this->assertAttributeSame(['o'], 'foundLetters', $game);
        $this->assertSame(
            [
                'word' => 'coucou',
                'attempts' => 2,
                'found_letters' => ['o'],
                'tried_letters' => ['a'],
            ],
            $game->getContext()
        );
    }
}
