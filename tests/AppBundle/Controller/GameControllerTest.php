<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GameControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/game/');
        $attempts = $crawler->filter('p.attempts');

        $this->assertContains(
            'You still have 11 remaining attempts.',
            $attempts->first()->text()
        );
    }


    public function testGuessWord()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/game/');
        $form = $crawler->selectButton('Let me guess...')->form();
        $client->followRedirects();
        $crawler = $client->submit($form, [
            'word' => 'php',
        ]);

        $this->assertContains(
            'You found the word php and won this game.',
            $crawler->filter('#content p')->first()->text()
        );
    }

    public function testGuessWithLetters()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/game/');
        $client->followRedirects();

        $link = $crawler->selectLink('P')->link();
        $client->click($link);

        $link = $crawler->selectLink('H')->link();
        $crawler = $client->click($link);

        $this->assertContains(
            'You found the word php and won this game.',
            $crawler->filter('#content p')->first()->text()
        );
    }
}
