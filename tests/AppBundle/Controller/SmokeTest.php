<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SmokeTest extends WebTestCase
{
    /**
     * @dataProvider urls
     */
    public function testPages($method, $url, $responseCode)
    {
        $client = static::createClient();

        $client->request($method, $url);
        $this->assertSame($responseCode, $client->getResponse()->getStatusCode());
    }

    public function urls()
    {
        yield ['GET', '/', 302];
        yield ['GET', '/en/game/', 200];
        yield ['GET', '/en/contact-us', 200];
    }
}
