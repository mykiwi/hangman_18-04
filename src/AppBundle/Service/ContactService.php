<?php

namespace AppBundle\Service;

use AppBundle\Entity\Contact;

class ContactService
{
    private $mailer;
    private $recipient;

    public function __construct(\Swift_Mailer $mailer, $recipient)
    {
        $this->mailer = $mailer;
        $this->recipient = $recipient;
    }

    public function sendMail(Contact $contact)
    {
        $message = (new \Swift_Message($contact->subject))
            ->setFrom($contact->email)
            ->setTo($this->recipient)
            ->setBody($contact->message)
        ;

        $this->mailer->send($message);
    }
}
