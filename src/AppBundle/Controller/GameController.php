<?php

namespace AppBundle\Controller;

use AppBundle\Game\GameContext;
use AppBundle\Game\GameRunner;
use AppBundle\Game\Loader\TextFileLoader;
use AppBundle\Game\Loader\XmlFileLoader;
use AppBundle\Game\WordList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/game", name="game_")
 * @Security("is_granted('PLAY_GAME')")
 */
class GameController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $game = $this->get(GameRunner::class)->loadGame($this->getParameter('word_lenght'));

        if ($game->isOver()) {
            return $this->redirectToRoute(
                $game->isWon()
                    ? 'game_won'
                    : 'game_failed'
            );
        }

        return $this->render('game/index.html.twig', [
            'game' => $game,
        ]);
    }

    /**
     * @Route("/tryLetter/{letter}", name="try_letter")
     */
    public function tryLetter($letter)
    {
        $this->get(GameRunner::class)->playLetter($letter);

        return $this->redirectToRoute('game_index');
    }

    /**
     * @Route("/tryWord", name="try_word", methods={"POST"})
     */
    public function tryWord(Request $request)
    {
        $gameRunner = $this->get(GameRunner::class);
        $game = $gameRunner->loadGame();
        $gameRunner->playWord($request->get('word'));

        return $this->redirectToRoute(
            $game->isWon()
                ? 'game_won'
                : 'game_failed'
        );
    }

    /**
     * @Route("/reset", name="reset")
     */
    public function reset()
    {
        $this->get(GameRunner::class)->resetGame();

        return $this->redirectToRoute('game_index');
    }

    /**
     * @Route("/won", name="won")
     */
    public function gameWon()
    {
        $gameRunner = $this->get(GameRunner::class);
        $word = $gameRunner->loadGame()->getWord();

        try {
            $gameRunner->resetGameOnSuccess();
        } catch (NotFoundHttpException $e) {
            return $this->redirectToRoute('game_index');
        }

        return $this->render('game/won.html.twig', [
            'word' => $word,
        ]);
    }

    /**
     * @Route("/loose", name="failed")
     */
    public function gameFailed()
    {
        $gameRunner = $this->get(GameRunner::class);
        $word = $gameRunner->loadGame()->getWord();

        try {
            $gameRunner->resetGameOnFailure();
        } catch (NotFoundHttpException $e) {
            return $this->redirectToRoute('game_index');
        }

        return $this->render('game/failed.html.twig', [
            'word' => $word,
        ]);
    }
}


























