<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="user_login")
     */
    public function login()
    {
        $securityUtils = $this->get('security.authentication_utils');

        return $this->render('security/login.html.twig', [
            'last_login' => $securityUtils->getLastUsername(),
            'error' => $securityUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        die('NOP');
    }
}
