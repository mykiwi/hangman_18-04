<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends Controller
{
    /**
     * @Route("/register", name="user_register")
     */
    public function register(Request $request)
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'User registered !');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('user/register.html.twig', [
            'register_user' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users", name="user_list")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function listUsers(Request $request)
    {
        $userRepository = $this->getDoctrine()
            ->getRepository(User::class)
        ;
        $users = $userRepository->findAll();

        $response = new Response();
        $response->setEtag(count($users));
        if ($response->isNotModified($request)) {
            return $response;
        }

        $response->setContent($this->renderView('user/list.html.twig', [
            'users' => $users,
        ]));

        return $response;
    }
}
