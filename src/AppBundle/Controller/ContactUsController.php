<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;
use AppBundle\Service\ContactService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactUsController extends Controller
{
    /**
     * @Route("/contact-us", name="contact")
     */
    public function contact(Request $request)
    {
        $contact = new Contact();
        $contact->message = 'Bonjour, ';
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get(ContactService::class)->sendMail($contact);

            $this->addFlash('success', 'contact_us.thanks');

            return $this->redirectToRoute('contact');
        }

        return $this->render('contact-us.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
