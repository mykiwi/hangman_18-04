<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="5")
     */
    public $subject;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="10")
     */
    public $message;
}
